## ATLNTX Starter-kit
Atlntx starter kit for Front End Development

## Installation
`npm install --save-dev`
`bower install`
If you happen to face node-sass error, try to run `npm rebuild node-sass`

## Gulp Command
`gulp serve` to run Development environment and Live reload
`gulp serve:dist` to run build and serve from dist folder

## Working on Projects
Write codes on app folders, `main.sass` are for importing partial sass files.
Use `jquery-include` to use partial html files.
Example of `jquery-include`:
````
<main role="main">
    <div class="include" data-path="templates/main.html">
    </div>
</main>
````

#### Contact <jaco.ahmad@gmail.com> or contact me on slack (if you're atlntx developer) if you have any question.