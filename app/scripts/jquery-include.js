(function ($) {
	$.include = function (domScript, callback) {
		var includeLen = $('div.include').length;
		var _timer;
		$('div.include').each(function (index, element) {
			var path = $(element).data('path');
			$.get(path, function (data) {
				$(element).after(data).remove();
				//console.log(path + ' ::loaded');
				if (--includeLen === 0) {
					loadDomScript(domScript);
					clearTimeout(_timer);
					_timer = setTimeout(function () {
						callback();
					}, 200);
				}
			});
		});
	};

	function loadDomScript(domScript) {
		while(domScript.length) {
			var domScriptPath = domScript.shift();
			$.getScript(domScriptPath);
		}
	}
})(jQuery);
